import os
from flask_restplus import Resource
import boto3
import uuid


class S3Service(Resource):

    # Import CSV/Excel file and return dataframe.
    @classmethod
    def upload(cls, file):
        s3 = boto3.client(
            "s3",
            aws_access_key_id=os.environ.get('AWS_ACCESS_KEY_ID'),
            aws_secret_access_key=os.environ.get('AWS_SECRET_ACCESS_KEY')
        )
        random = uuid.uuid1()
        fileName = "{}-{}".format(random, file.filename.replace(" ", "-"))

        s3.upload_fileobj(
            file,
            os.environ.get('S3_BUCKET_NAME'),
            fileName,
            ExtraArgs={
                "ACL": "public-read",
                "ContentType": file.content_type
            }
        )

        s3BucketUrl = os.environ.get('S3_LOCATION').format(os.environ.get('S3_BUCKET_NAME'))
        return "{}{}".format(s3BucketUrl, fileName)
