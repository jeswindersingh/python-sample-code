import json

from sqlalchemy.exc import SQLAlchemyError

from app.main import db
from app.main.config.global_config import GlobalConfig
from app.main.exceptions.exception import InvalidUsage
from app.main.helpers import format_column_name, if_exists_return
from app.main.model.user_table import UserTblsModel
from app.main.model.user_table_column import UserTblsColsModel
from app.main.model.user_table_column_index import UserTblsColsIndexModel
from app.main.service.master_table_service import execute_query


class UserTableColumnRepository:
    def save_bulk(self, columns_payload):
        objects = []
        for column_payload in columns_payload:
            obj = self.set_attribute(column_payload)
            objects.append(obj)

        try:
            db.session.add_all(objects)
            db.session.commit()
            return self
        except SQLAlchemyError as e:
            db.session.rollback()
            db.session.close()
            error = str(e.__dict__['orig'])
            raise InvalidUsage(error, 422)

        return self

    def set_attribute(self, payload) -> UserTblsColsModel:
        obj = UserTblsColsModel(
            user_table_id=payload['table_id'],
            name=payload['name'],
            display_name=payload['display_name'] if 'display_name' in payload else payload['new_name'],
            data_type=payload['data_type'],
            width=if_exists_return(payload, 'width', 100),
            date_format=if_exists_return(payload, 'date_format', None)
        )

        return obj

    def create_idx_on_col(self, col_id, user_id):
        column = UserTblsColsModel.find_by_id(col_id)
        table = column.user_table

        # check if index exists in table
        index_exists = UserTblsColsIndexModel.find_by_table_and_col_id(table_id=table.id, col_id=col_id)

        if index_exists is None:
            index_name = f"idx_{table.name}_{column.name}"
            if len(index_name) > GlobalConfig.max_index_name_length:
                index_name = index_name[0:GlobalConfig.max_index_name_length]

            user_table_col_index = UserTblsColsIndexModel(user_id=user_id, table_id=table.id,
                                                          table_column_id=column.id, name=index_name)
            user_table_col_index.save_to_db()

            create_index = f"ALTER TABLE `{table.name}` ADD INDEX `{index_name}` (`{column.name}`)"
            execute_query(create_index)
